import 'package:app_coin_wash_manager/components/component_appbar_filter.dart';
import 'package:app_coin_wash_manager/components/component_appbar_normal.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/pages/page_detail.dart';
import 'package:app_coin_wash_manager/pages/page_form.dart';
import 'package:app_coin_wash_manager/pages/page_list.dart';
import 'package:flutter/material.dart';

class PageMachine extends StatefulWidget {
  const PageMachine({Key? key}) : super(key: key);

  @override
  State<PageMachine> createState() => _PageMachineState();
}

class _PageMachineState extends State<PageMachine> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '기계관리',
      ),
      body: Center(
        child: Column(
          children: [
            Image.asset('assets/kakao4.jpeg'),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(colorWash),
              ),
              onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PageForm(
                  ),
                ),
              );
            }, child: Text(
              '기계정보 등록',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 20,
              ),
            ),
            ),
            SizedBox(height: 30,),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(colorWash),
              ),
              onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PageList(
                  ),
                ),
              );
            }, child: Text(
              '기계 리스트',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 20,
              ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}
