
import 'package:app_coin_wash_manager/components/component_appbar_popup.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_form_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageUsageSearch extends StatefulWidget {
   const PageUsageSearch({super.key, required this.dateStart, required this.dateEnd});

  final String dateStart;
  final String  dateEnd;

  @override
  State<PageUsageSearch> createState() => _PageUsageSearchState();
}

class _PageUsageSearchState extends State<PageUsageSearch> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
          title: '세탁기 이용내역 조회'
      ),
      body: Center(
        child: _buildBody(),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: OutlinedButton(

            style: ButtonStyle(

            ),
            onPressed: () {
              String dateStart = DateFormat('yyyy-MM-dd').format(
                  _formKey.currentState!.fields['dateStart']!.value);
              String dateEnd = DateFormat('yyyy-MM-dd').format(
              _formKey.currentState!.fields['dateEnd']!.value);

                Navigator.pop(
                  context,
                  [true, dateStart, dateEnd],
                );
              },
            child: const Text('검색하기',
            style: TextStyle(
              fontFamily: 'naverTitle',
              color: colorWash
            ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        SizedBox(
          height: 20,
        ),
        FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                FormBuilderDateTimePicker(
                name: 'dateStart',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                    color: colorWash
                  ),
                  labelText: '이용내역 시작일',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorWash),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),

                    onPressed: () {
                      _formKey.currentState!.fields['dateStart']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
                SizedBox(
                  height: 30,
                ),
                FormBuilderDateTimePicker(
                name: 'dateEnd',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                      fontFamily: 'maple',
                      color: colorWash
                  ),
                  labelText: '이용내역 마지막일',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    borderSide: BorderSide(color: colorWash),
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),

                    onPressed: () {
                      _formKey.currentState!.fields['dateEnd']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
