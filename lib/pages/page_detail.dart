
import 'package:app_coin_wash_manager/components/component_appbar_popup.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_no_contents.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/machine_detail.dart';
import 'package:app_coin_wash_manager/pages/page_form.dart';
import 'package:app_coin_wash_manager/repository/repo_machine.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageDetail extends StatefulWidget {
  const PageDetail({super.key, required this.machineId});
  /*
  이 창이 호출될 때 머신아이디가 꼭 필요함.
  머신아이디를 받아와서 기계 정보 단일 조회(get) api를 다시 호출 할 것임.
   */
  final int machineId;

  @override
  State<PageDetail> createState() => _PageDetailState();
}

class _PageDetailState extends State<PageDetail> {
  // api에서 받아온 숫자를 예쁘게 보여주기 위한 포맷터. 100000 -> 100,000 이런식으로 바꿔줌.
  final NumberFormat nFormat = NumberFormat('#,###');

  // list는 초기값으로 [] 빈배열로 넣어주고 모델같은 경우는 이런식으로 초기화 해줌. 숫자는 0, 문자는 ''
  MachineDetail _detail = MachineDetail(0, '', '', '', 0);

  @override
  void initState() {
    super.initState();
    _getDetail();
  }

  Future<void> _delData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMachine().delData(widget.machineId).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(success: true,
          title: '데이터를 삭제 했습니다',
          subTitle: res.msg,
      ).call();

      Navigator.pop(context); // 다이알로그 창 닫기
      Navigator.pop(context, [true]); // 상세페이지 닫으면서 부모페이지(리스트 페이지) 에 새로고침 해야한다고 말
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(success: false,
          title: '데이터 삭제 실패',
          subTitle: '삭제 실패'
      ).call();

      // 삭제가 실패했으므로 현재창 닫지않음
      Navigator.pop(context);  // 다이알로그 창 닫기
    });
  }


  // 이 창이 호출될 때 머신아이디를 받아왔으므로 그걸 이용해서 삭제 구현
  Future<void> _getDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().getDetail(widget.machineId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      // 호출실패 시 창을 닫아버림. (데이터가 없는 시퀀스 혹은 인터넷 오류로 이 창을 유지시켜봤자
      // 의미가 없음.
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '기계 상세정보',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (_detail.id != 0) {
      return

          Center(
            child: Column(
              children: [
                SizedBox(height: 30,),
                Text('고유번호 : ${_detail.id}',style: TextStyle(fontFamily: 'kakao',fontSize: 20),),
                Divider(thickness: 2,color: colorWash, indent: 30, endIndent: 30,),
                Text('기계이름 : ${_detail.machineName}',style: TextStyle(fontFamily: 'kakao',fontSize: 20),),
                Divider(thickness: 2,color: colorWash, indent: 30, endIndent: 30,),
                Text('구매금액 : ${nFormat.format(_detail.machinePrice)}',style: TextStyle(fontFamily: 'kakao',fontSize: 20),),
                Divider(thickness: 2,color: colorWash, indent: 30, endIndent: 30,),
                Text('기계타입 : ${_detail.machineTypeName}',style: TextStyle(fontFamily: 'kakao',fontSize: 20),),
                Divider(thickness: 2,color: colorWash, indent: 30, endIndent: 30,),
                Text('구매일 : ${_detail.datePurchase}',style: TextStyle(fontFamily: 'kakao',fontSize: 20),),
                Divider(thickness: 2,color: colorWash, indent: 30, endIndent: 30,),
           ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(colorWash)
              ),
              onPressed: () async {
                final popup = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PageForm(
                          machineDetail: _detail,
                        )));

                if (popup != null && popup[0]) {
                  Navigator.pop(context, [true]);
                }
              },
              child: const Text('수정',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),

          SizedBox(
            height: 30,
          ),
          ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.red),
              ),
              onPressed: () {
                _showDeleteDialog();
              },
              child: Text('삭제',
                style: TextStyle(
                  fontFamily: 'maple',
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),
              ],
          ),
      );
    } else {
      return const ComponentNoContents(
          icon: Icons.not_interested, msg: '기계정보가 없습니다.');
    }
  }

  void _showDeleteDialog() {     // 다이알로그 창 띄우기
    showDialog(context: context,
        barrierDismissible: true,  //뒷배경의 touchEvent 가능여
        builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('기계정보 삭제'), // 제목
        content: const Text('정말 이 정보를 삭제하시겠습니까?'),  // 부제목
        actions: [
          OutlinedButton(
              onPressed: () {
                _delData();  //위에 만들어둔 삭제 메소드
              },
              child: const Text('확인',
              style: TextStyle(
                fontFamily: 'kakao'
              ),
              )
          ),
          OutlinedButton(
              onPressed: () {
                Navigator.pop(context);  // 취소일땐 삭제하면 안돼기 때문에 pop
              },
              child: const Text('취소',
                style: TextStyle(
                    fontFamily: 'kakao'
                ),
              )
          ),
        ],
      );
        }
    );
  }
 }