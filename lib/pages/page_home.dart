import 'package:app_coin_wash_manager/components/component_appbar_normal.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:flutter/material.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(
        child: Container(
          child: Column(
            children: [
              Image.asset('assets/kakao.png'),
              //   Image.asset(
              //     'assets/dojun.jpeg'
              // ),
              Text('"Well Come!!!" \n Coin Laundromat',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'kakao',
                  fontSize: 30,
                  color: colorKakaoB,
                ),),
              SizedBox(height: 30,),
              Image.asset('assets/kakao3.jpeg'),

            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}
