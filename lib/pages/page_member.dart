import 'package:app_coin_wash_manager/components/component_appbar_normal.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/member_join_request.dart';
import 'package:app_coin_wash_manager/pages/page_memeber_list.dart';
import 'package:app_coin_wash_manager/pages/page_set_member.dart';
import 'package:app_coin_wash_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMember extends StatefulWidget {
  const PageMember({Key? key}) : super(key: key);

  @override
  State<PageMember> createState() => _PageMemberState();
}

class _PageMemberState extends State<PageMember> {
  final _scrollController = ScrollController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '회원 관리',
      ),
      body: Center(
        child: Container(
          child: Column(
            children: [
              Image.asset('assets/coin.jpeg',
           ),
              ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorWash),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PageSetMember(
                        ),
                      ),
                    );
                  },
                  child: Text('회원 등록',
                    style: TextStyle(
                      fontFamily: 'maple',
                      fontSize: 20,
                    ),
                  ),
              ),
              SizedBox(height: 20,),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(colorWash),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageMemberList(
                      ),
                    ),
                  );
                },
                child: Text('회원 정보',
                  style: TextStyle(
                    fontFamily: 'maple',
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}
