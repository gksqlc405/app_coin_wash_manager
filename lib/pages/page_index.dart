import 'package:app_coin_wash_manager/components/component_appbar_filter.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/pages/page_form.dart';
import 'package:app_coin_wash_manager/pages/page_home.dart';
import 'package:app_coin_wash_manager/pages/page_machine.dart';
import 'package:app_coin_wash_manager/pages/page_member.dart';
import 'package:app_coin_wash_manager/pages/page_usage_date.dart';

import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentTabIndex = 0;


  final List<Widget> _widgetOptions = <Widget>[
    PageHome(),
    PageMachine(),
    PageMember(),
    PageUsageDate(),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      _buildNavigationBar(),
    );
  }

  Widget _buildNavigationBar() {
    final _widgetOptions = <Widget>[
      PageHome(),
      PageMachine(),
      PageMember(),
      PageUsageDate(),
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      const BottomNavigationBarItem(icon: Icon(Icons.home,color: colorWash,), label: 'Home'),
      const BottomNavigationBarItem(icon: Icon(Icons.adb,color: colorWash,), label: '기계관리'),
      const BottomNavigationBarItem(icon: Icon(Icons.person_pin_outlined,color: colorWash,), label: '회원관리'),
      const BottomNavigationBarItem(icon: Icon(Icons.calendar_month_outlined,color: colorWash,), label: '이용내역 날짜'),
    ];
    assert(_widgetOptions.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      selectedItemColor: colorWash,
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      body: _widgetOptions[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
