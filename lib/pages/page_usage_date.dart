import 'dart:core';
import 'package:app_coin_wash_manager/components/component_appbar_filter.dart';
import 'package:app_coin_wash_manager/components/component_count_title.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_list_item.dart';
import 'package:app_coin_wash_manager/components/component_no_contents.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/usage_details_item.dart';
import 'package:app_coin_wash_manager/pages/page_usage_seach.dart';
import 'package:app_coin_wash_manager/repository/repo_member.dart';
import 'package:intl/intl.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageUsageDate extends StatefulWidget {
  const PageUsageDate({Key? key}) : super(key: key);

  @override
  State<PageUsageDate> createState() => _PageUsageDateState();
}

class _PageUsageDateState extends State<PageUsageDate> with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  final _scrollController = ScrollController();



  List<UsageDetailsItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _dateStart =  DateFormat('yyyy-MM-dd').format(DateTime.now());
  String _dateEnd = DateFormat('yyyy-MM-dd').format(DateTime.now());


  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) { // 시작하기전에 미리 띄우고
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });


      await RepoMember()
          .getUsageList(dateStart: _dateStart, dateEnd: _dateEnd)
          .then((res) {
        BotToast.closeAllLoading(); // 성공했을때도 닫고

        setState(() {
          _totalPage = res.totalPage;
          _totalItemCount = res.totalItemCount;


          _list = [..._list, ...res.list];

          _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading());

    }

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '세탁기 이용내역 조회',
        actionIcon: Icons.search,
        callback: () async {
          final searchResult = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      PageUsageSearch(
                          dateStart: _dateStart, dateEnd: _dateEnd)
              )
          );

          if (searchResult != null && searchResult[0]) {
            _dateStart = searchResult[1];
            _dateEnd = searchResult[2];
            _loadItems(reFresh: true);
          }
        },
      ),
      body: Center(
        child: ListView(
          controller: _scrollController,
          children: [
            SizedBox(
              height: 30,
            ),
            ComponentCountTitle(icon: Icons.work_history_outlined,
                count: _totalItemCount,
                unitName: '건',
                itemName: '기계사용 정보'
            ),
            Divider(
              thickness: 1,
              color: colorWash,
            ),

            Image.asset('assets/kakao3.jpeg',
              width: 200,
              height: 200,
            ),
            _buildBody(),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }


  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Center(
        child:
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentListItem(item: _list[index]),
            )
          ],
        ),
      );
    } else {
      return SizedBox(

        height: MediaQuery.of(context).size.height - 200 - 150,
        child: const ComponentNoContents(icon: Icons.adb, msg: '출퇴근 날짜 정보가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}