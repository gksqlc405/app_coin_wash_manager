import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_form_validator.dart';
import 'package:app_coin_wash_manager/model/member_join_request.dart';
import 'package:app_coin_wash_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageSetMember extends StatefulWidget {
  const PageSetMember({Key? key}) : super(key: key);

  @override
  State<PageSetMember> createState() => _PageSetMemberState();
}

class _PageSetMemberState extends State<PageSetMember> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setMember(MemberJoinRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMember().setMember(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '회원 등록이 완료되었습니다',
        subTitle: res.msg,
      ).call();

      Navigator.pop(
          context,
          [true]
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '회원 등록에 실패였습니다',
        subTitle: '올바르게 입력했는지 확인해 주십시오',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(colorWash)
          ),
          onPressed: () {
            if (_formKey.currentState?.saveAndValidate() ?? false) {
                MemberJoinRequest request = MemberJoinRequest(
                  _formKey.currentState!.fields['memberName']!.value,
                  _formKey.currentState!.fields['memberPhone']!.value,
                  _formKey.currentState!.fields['birthday']!.value,
                );
                _setMember(request);
            }
          },
          child: Text('등록',
            style: TextStyle(
              fontFamily: 'kakao',
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FormBuilderTextField(
                name: 'memberName',
                decoration: const InputDecoration(
                  labelText: '회원 이름',
                  labelStyle: TextStyle(
                      fontFamily: 'kakao',
                      color: colorWash
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorWash),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'memberPhone',
                decoration: const InputDecoration(
                  labelText: '전화번호',
                  hintText: '- 는 필수입니다',
                  suffixIcon: Text('010-xxxx-xxxx',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 13,
                      color: colorWash,
                    ),
                  ),
                  labelStyle: TextStyle(
                      fontFamily: 'kakao',
                      color: colorWash
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorWash),
                  ),
                ),
                validator: FormBuilderValidators.compose([  // compose = 합성, 구성, 창작하다
                  FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
                ]),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'birthday',
                decoration: const InputDecoration(
                  labelText: '생년월일',
                  hintText: '- 는 필수입니다',
                  suffixIcon: Text('ex) 1995-11-24',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontFamily: 'maple',
                      fontSize: 13,
                      color: colorWash,
                    ),
                  ),
                  labelStyle: TextStyle(
                      fontFamily: 'kakao',
                      color: colorWash
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorWash),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
