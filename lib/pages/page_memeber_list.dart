import 'package:app_coin_wash_manager/components/component_appbar_actions.dart';
import 'package:app_coin_wash_manager/components/component_count_title.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_member_item.dart';
import 'package:app_coin_wash_manager/components/component_no_contents.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/member_item.dart';
import 'package:app_coin_wash_manager/pages/page_set_member.dart';
import 'package:app_coin_wash_manager/repository/repo_member.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMemberList extends StatefulWidget {
  const PageMemberList({Key? key}) : super(key: key);

  @override
  State<PageMemberList> createState() => _PageMemberListState();
}

class _PageMemberListState extends State<PageMemberList> {
  final _scrollController = ScrollController();

  List<MemberItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getMemberList();
  }

  Future<void> _getMemberList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().getMemberList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      // 이번에 추가된 알림창 컴포넌트. component_notification 참고
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '회원 정보',
        isUseActionBtn1: true,
        action1Icon: Icons.add,
        action1Callback: () async {
          final popup = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PageSetMember())
          );

          if (popup != null && popup[0]) {
            _getMemberList();

          }
        },
        isUseActionBtn2: true,
        action2Icon: Icons.refresh,
        action2Callback: () {
          _getMemberList();
        },
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          SizedBox(height: 30,),
          ComponentCountTitle(icon: Icons.list, count: _totalItemCount, unitName: '명', itemName: '회원정보'),
          Divider(
            thickness: 2,
            color: colorWash,
          ),
          SizedBox(
            height: 20,
          ),
          _buildList(),
        ],
      ),
    );
  }

  Widget _buildList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMemberItem(
              memberItem: _list[index],

            ),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다.'),
      );
    }
  }
}
