
import 'package:app_coin_wash_manager/components/component_appbar_popup.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_dropdown.dart';
import 'package:app_coin_wash_manager/config/config_form_validator.dart';
import 'package:app_coin_wash_manager/model/machine_detail.dart';
import 'package:app_coin_wash_manager/model/machine_request.dart';
import 'package:app_coin_wash_manager/model/machine_name_update_request.dart';
import 'package:app_coin_wash_manager/repository/repo_machine.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageForm extends StatefulWidget {
  PageForm({super.key, this.machineDetail});


  MachineDetail? machineDetail;

  @override
  State<PageForm> createState() => _PageFormState();
}

class _PageFormState extends State<PageForm> {


  final _formKey = GlobalKey<FormBuilderState>();


  Future<void> _putMachine(int machineNameId,MachineNameUpdateRequest request) async {

    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().putData(machineNameId,request).then((res) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: true,
        title: '기계정보 수정에 성공하였습니다',
        subTitle: res.msg,
      ).call();


      Navigator.pop(context, [true]
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '기계 수정 실패',
          subTitle: '기계 수정에 실패하였습니다'
      ).call();
    });
  }



  Future<void> _setMachine(MachineRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMachine().setData(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀 로딩 닫고

      ComponentNotification(
        success: true,
        title: '기계 등록 성공',
        subTitle: res.msg, //백엔드에서 반환받은 메세지 넣기
      ).call();

      //창 닫으면서 다 처리하고 정상적으로 닫혔다 !!!!! 라고 알려준다
      Navigator.pop(
          context,
          [true]    // <- 다 처리하고 정상적으로 닫혔다는 의미
      );
    }).catchError((err) {
      BotToast.closeAllLoading();  // 커스텀로딩 닫기

      ComponentNotification(
        success: false,
        title: '기계정보등록 실패',
        subTitle: '기계등록에 실패 하였습니다',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '기계 ${widget.machineDetail == null ? '등록' : '수정'}'),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(colorWash)
          ),
          onPressed: () {
            if (_formKey.currentState?.saveAndValidate() ?? false) {
              if (widget.machineDetail == null) {

                MachineRequest request = MachineRequest(
                  _formKey.currentState!.fields['machineType']!.value,
                  _formKey.currentState!.fields['machineName']!.value,
                  DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['datePurchase']!.value),
                  double.parse(_formKey.currentState!.fields['machinePrice']!.value),
                );

                _setMachine(request);
              } else {
                MachineNameUpdateRequest request = MachineNameUpdateRequest(
                  _formKey.currentState!.fields['machineName']!.value,
                );
               _putMachine(widget.machineDetail!.id, request);
              }
            }
          },
          child: Text(widget.machineDetail == null ? '등록' : '수정',
            style: TextStyle(
              fontFamily: 'kakao',
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }




  Widget _buildBody() {
    return SingleChildScrollView(  // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        initialValue: widget.machineDetail == null ? {} : {
          'machineName' : widget.machineDetail!.machineName
        },
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child:  Column(
            children: [
              FormBuilderTextField(
                name: 'machineName',
                decoration: const InputDecoration(
                  labelText: '기계이름',
                  labelStyle: TextStyle(
                      fontFamily: 'kakao',
                      color: colorWash
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorWash),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired), // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)), // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),// err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 20,),
              // set일때 입력창 넣기
              if (widget.machineDetail == null)
                FormBuilderTextField(
                  name: 'machinePrice',
                  decoration: const InputDecoration(
                    labelText: '기계가격',
                    labelStyle: TextStyle(
                        fontFamily: 'kakao',
                        color: colorWash
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: colorWash),
                    ),
                  ),
                  readOnly: widget.machineDetail == null ? false : true,
                  validator: FormBuilderValidators.compose([  // compose = 합성, 구성, 창작하다
                    FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
                    FormBuilderValidators.numeric(errorText: formErrorNumeric), // err 숫자만 입력해주세요.
                  ]),
                  keyboardType: TextInputType.number,
                ),
              SizedBox(height: 20,),
              if (widget.machineDetail == null)
                FormBuilderDropdown(
                  name: 'machineType',
                  decoration: const InputDecoration(
                    labelText: '기계종류',
                    labelStyle: TextStyle(
                        fontFamily: 'kakao',
                        color: colorWash
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: colorWash),
                    ),
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),  //err 이 값은 필수입니다
                  ]),
                  items: dropdownMachineType,
                ),
              SizedBox(height: 20,),
              if (widget.machineDetail == null)
                FormBuilderDateTimePicker(
                  name: 'datePurchase',
                  format: DateFormat('yyyy-MM-dd'),
                  inputType: InputType.date,
                  decoration: InputDecoration(
                    labelText: '구매일',
                    labelStyle: TextStyle(
                        fontFamily: 'kakao',
                        color: colorWash
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: colorWash),
                    ),
                    suffixIcon: IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        _formKey.currentState!.fields['datePurchase']?.didChange(null);
                      },
                    ),
                  ),
                  locale: const Locale.fromSubtags(languageCode: 'ko'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                  ]),
                ),

              //put 일땐 뿌리기만 하기
              if (widget.machineDetail != null)
                Text('기계가격 : ${widget.machineDetail!.machinePrice}',style: TextStyle(fontFamily: 'kakao', fontSize: 20),),
              SizedBox(height: 20,),
              if (widget.machineDetail != null)
                Text('기계종류 : ${widget.machineDetail!.machineTypeName}',style: TextStyle(fontFamily: 'kakao', fontSize: 20),),
              SizedBox(height: 20,),
              if (widget.machineDetail != null)
                Text('구매일 : ${widget.machineDetail!.datePurchase}',style: TextStyle(fontFamily: 'kakao', fontSize: 20),),
            ],
          ),
        ),
      ),
    );
  }
}


