import 'package:app_coin_wash_manager/config/config_api.dart';
import 'package:app_coin_wash_manager/model/common_result.dart';
import 'package:app_coin_wash_manager/model/member_join_request.dart';
import 'package:app_coin_wash_manager/model/member_list_result.dart';
import 'package:app_coin_wash_manager/model/usage_list_result.dart';
import 'package:dio/dio.dart';

class RepoMember {


  Future<CommonResult> setMember(MemberJoinRequest request) async {
    const String baseUrl = '$apiUrl/member/new';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<MemberListResult> getMemberList() async {
    const String baseUrl = '$apiUrl/member/all';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return MemberListResult.fromJson(response.data);
  }


  Future<UsageListResult> getUsageList({String dateStart = '' , String dateEnd = ''}) async {
    const String baseUrl = '$apiUrl/usage-details/search';
    Map<String, dynamic> params = {};
    params['dateStart'] = dateStart;
    params['dateEnd'] = dateEnd;

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll(dateStart,dateStart.toString()).replaceAll(dateEnd, dateEnd.toString()),
        queryParameters: params,
        options: Options(
          followRedirects: false,
        )
    );

    return UsageListResult.fromJson(response.data);
  }
}