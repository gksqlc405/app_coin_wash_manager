
import 'package:app_coin_wash_manager/config/config_api.dart';
import 'package:app_coin_wash_manager/model/common_result.dart';
import 'package:app_coin_wash_manager/model/machine_detail_response.dart';
import 'package:app_coin_wash_manager/model/machine_list_result.dart';
import 'package:app_coin_wash_manager/model/machine_request.dart';
import 'package:app_coin_wash_manager/model/machine_name_update_request.dart';
import 'package:app_coin_wash_manager/model/member_join_request.dart';
import 'package:dio/dio.dart';

class RepoMachine {

  Future<CommonResult> setMember(MemberJoinRequest request) async {
    const String baseUrl = '$apiUrl/member/new';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> setData(MachineRequest request) async {
    const String baseUrl = '$apiUrl/machine/new';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delData(int machineId) async {
    const String baseUrl = '$apiUrl/machine/delete/{machineId}';

    Dio dio = Dio();

    final response = await dio.delete(
      baseUrl.replaceAll('{machineId}', machineId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    
    return CommonResult.fromJson(response.data);
  }


  Future<CommonResult> putData(int machineNameId, MachineNameUpdateRequest request) async {
    const String baseUrl = '$apiUrl/machine/machineName/{machineNameId}';

    Dio dio = Dio();

    final response = await dio.put(
      baseUrl.replaceAll('{machineNameId}', machineNameId.toString()),  // replaceAll = 교체하다
      data: request.toJson(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );

    return CommonResult.fromJson(response.data);
  }


  Future<MachineListResult> getList({String machineType = ''}) async {
    const String baseUrl = '$apiUrl/machine/search';

    Map<String, dynamic> params = {};
    if (machineType != '') params['machineType'] = machineType;

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
      queryParameters: params,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );

    return MachineListResult.fromJson(response.data);
  }

  Future<MachineDetailResponse> getDetail(int machineId) async {
    const String baseUrl = '$apiUrl/machine/{id}';

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl.replaceAll('{id}', machineId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      ),
    );

    return MachineDetailResponse.fromJson(response.data);
  }
}