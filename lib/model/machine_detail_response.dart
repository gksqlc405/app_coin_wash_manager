import 'package:app_coin_wash_manager/model/machine_detail.dart';

class MachineDetailResponse {
  MachineDetail date;
  bool isSuccess;
  int code;
  String msg;

  MachineDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory MachineDetailResponse.fromJson(Map<String, dynamic> json) {
    return MachineDetailResponse(
      MachineDetail.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
