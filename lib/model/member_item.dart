class MemberItem {
  int id;
  String memberName;
  String memberPhone;
  String birthday;
  String isEnable;
  String dateJoin;
  String dateWithdrawal;

  MemberItem(this.id, this.memberName, this.memberPhone, this.birthday, this.isEnable, this.dateJoin, this.dateWithdrawal);

  factory MemberItem.fromJson(Map<String, dynamic> json) {
    return MemberItem(
      json['id'],
      json['memberName'],
      json['memberPhone'],
      json['birthday'],
      json['isEnable'],
      json['dateJoin'],
      json['dateWithdrawal'],

    );
  }
}