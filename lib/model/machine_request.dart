class MachineRequest {
  String machineType;
  String machineName;
  String datePurchase;
  double machinePrice;

  MachineRequest(this.machineType, this.machineName, this.datePurchase, this.machinePrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['machineType'] = this.machineType;
    data['machineName'] = this.machineName;
    data['datePurchase'] = this.datePurchase;
    data['machinePrice'] = this.machinePrice;

    return data;
  }
}