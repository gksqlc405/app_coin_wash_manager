
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:app_coin_wash_manager/model/member_item.dart';

class MemberListResult {
  List<MemberItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MemberListResult(this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg);

  factory MemberListResult.fromJson(Map<String, dynamic> json) {
    return MemberListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MemberItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}