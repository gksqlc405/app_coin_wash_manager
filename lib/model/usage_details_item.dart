class UsageDetailsItem {
  int usageDetailsId;
  String dateUsage;
  int machineId;
  String machineFullName;
  int memberId;
  String memberName;
  String memberPhone;
  String memberBirthday;
  bool memberIsEnable;
  String memberDateJoin;
  String memberDateWithdrawal;

  UsageDetailsItem(
      this.usageDetailsId,
      this.dateUsage,
      this.machineId,
      this.machineFullName,
      this.memberId,
      this.memberName,
      this.memberPhone,
      this.memberBirthday,
      this.memberIsEnable,
      this.memberDateJoin,
      this.memberDateWithdrawal,
      );

  factory UsageDetailsItem.fromJson(Map<String, dynamic> json) {
    return UsageDetailsItem(
      json['usageDetailsId'],
      json['dateUsage'],
      json['machineId'],
      json['machineFullName'],
      json['memberId'],
      json['memberName'],
      json['memberPhone'],
      json['memberBirthday'],
      json['memberIsEnable'],
      json['memberDateJoin'],
      json['memberDateWithdrawal'],
    );
  }
}