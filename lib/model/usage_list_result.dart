
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:app_coin_wash_manager/model/member_item.dart';
import 'package:app_coin_wash_manager/model/usage_details_item.dart';

class UsageListResult {
  List<UsageDetailsItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  UsageListResult(this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg);

  factory UsageListResult.fromJson(Map<String, dynamic> json) {
    return UsageListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => UsageDetailsItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}