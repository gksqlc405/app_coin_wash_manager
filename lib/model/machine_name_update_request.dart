class MachineNameUpdateRequest {
  String machineName;

  MachineNameUpdateRequest(this.machineName);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['machineName'] = this.machineName;
    return data;
  }
}