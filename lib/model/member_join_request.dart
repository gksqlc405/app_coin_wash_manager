class MemberJoinRequest {
  String memberName;
  String memberPhone;
  String birthday;

  MemberJoinRequest(this.memberName, this.memberPhone, this.birthday);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['memberName'] = this.memberName;
    data['memberPhone'] = this.memberPhone;
    data['birthday'] = this.birthday;

    return data;

  }
}