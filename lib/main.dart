import 'package:app_coin_wash_manager/model/machine_detail.dart';
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:app_coin_wash_manager/pages/page_detail.dart';
import 'package:app_coin_wash_manager/pages/page_index.dart';
import 'package:app_coin_wash_manager/pages/page_list.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],

      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const PageIndex(),
    );
  }
}

