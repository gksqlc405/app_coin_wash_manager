
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:app_coin_wash_manager/model/member_item.dart';
import 'package:flutter/material.dart';

class ComponentMemberItem extends StatelessWidget {
  const ComponentMemberItem({super.key, required this.memberItem});

  final MemberItem memberItem;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 3.0,
            ),
          ],
          border: Border.all(
            color: colorWash,
            style: BorderStyle.solid,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text('${memberItem.id}번',
              style: TextStyle(
                fontFamily: 'maple',
                fontSize: 15,
              ),
            ),
            Text('이름 : ${memberItem.memberName}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            Text('생년월일 : ${memberItem.birthday}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            Text('전화번호 : ${memberItem.memberPhone}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            Text('회원 상태가 유효합니까 : ${memberItem.isEnable}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            Text('가입 날짜 : ${memberItem.dateJoin}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
