

import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/usage_details_item.dart';

import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,

  });

  final UsageDetailsItem item;



  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: colorWash,
              blurRadius: 1.0,
              spreadRadius: 1.0,
            ),
          ],
          border: Border.all(
            color: Colors.white,
            style: BorderStyle.solid,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(60.0),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Text('회원 이름 : ${item.memberName}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('전화번호 : ${item.memberPhone}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('${item.machineFullName}',
              style: TextStyle(
                fontFamily: 'kakao',
                fontSize: 15,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
