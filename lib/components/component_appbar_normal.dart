import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:flutter/material.dart';

class ComponentAppbarNormal extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarNormal({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: colorWash,
      centerTitle: true ,
      automaticallyImplyLeading: true,
      title: Text(title,
        style: TextStyle(
            fontFamily: 'kakao',
          // color: colorWash
        ),
      ),
      elevation: 5.0,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(45);
}
