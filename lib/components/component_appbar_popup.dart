import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:flutter/material.dart';

class ComponentAppbarPopup extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarPopup({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: colorWash,
      centerTitle: true,  // 앱바 타이틀명 중앙으로 할거니?
      automaticallyImplyLeading: false,  // 타이틀(글자) 앞에 자동으로 뒤로가기버튼 같은거 달리는거 허용하겠니?
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'kakao'
        ),
      ),
      elevation: 1,
      actions: [
        IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.clear)),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(45);
}
