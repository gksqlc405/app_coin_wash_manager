
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:flutter/material.dart';

class ComponentMachineItem extends StatelessWidget {
  const ComponentMachineItem({super.key, required this.machineItem, required this.callback});

  final MachineItem machineItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              spreadRadius: 3.0,
            ),
          ],
          border: Border.all(
            color: colorWash,
              style: BorderStyle.solid,
              width: 5,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Text(machineItem.machineFullName,
            style: TextStyle(
              fontFamily: 'maple',
              fontSize: 15,
            ),
            ),
            Text('구매일 : ${machineItem.datePurchase}',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            ),
          ],
        ),
      ),
    );
  }
}
