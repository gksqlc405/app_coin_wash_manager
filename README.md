# 코인 빨래방 (관리자) APP

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```
 
* 기게정보 관리(관리자)
  - 기계정보 상세페이지 (Get)
  - 기계정보 삭제 (Del)
  - 기계이름 수정 (Put)
  - 기계정보 등록 (Post)
  - 기계리스트 가저오기 (Get)
 
* 회원 관리(관리자)
  - 회원가입 정보 등록 (Post)
  - 회원 정보 모두가저오기 (Get)
  
  
* 이용내역(관리자) 
  - 이용내역 날짜 확인 (Get)

```


# app 화면

### Home 화면
![CoinWash](./images/coin1.png)

### 기계관리
![CoinWash](./images/coin2.png)

### 기계관리
![CoinWash](./images/coin3.png)

### 기계관리
![CoinWash](./images/coin4.png)